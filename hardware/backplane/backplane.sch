EESchema Schematic File Version 4
EELAYER 26 0
EELAYER END
$Descr A4 11693 8268
encoding utf-8
Sheet 1 1
Title ""
Date ""
Rev ""
Comp ""
Comment1 ""
Comment2 ""
Comment3 ""
Comment4 ""
$EndDescr
$Comp
L atmel:ATMEGA328P-MU U1
U 1 1 5A749BAD
P 4900 3200
F 0 "U1" H 4950 4567 50  0000 C CNN
F 1 "ATMEGA328P-MU" H 4950 4476 50  0000 C CNN
F 2 "MLF/QFN32" H 4900 3200 50  0001 C CIN
F 3 "http://www.atmel.com/images/atmel-8271-8-bit-avr-microcontroller-atmega48a-48pa-88a-88pa-168a-168pa-328-328p_datasheet.pdf" H 4900 3200 50  0001 C CNN
	1    4900 3200
	1    0    0    -1  
$EndComp
$EndSCHEMATC
