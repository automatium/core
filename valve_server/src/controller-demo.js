// @flow

const fs = require('fs')
const path = require('path')
const chalk = require('chalk')
const SerialPort = require('serialport')
const Buffer = require('buffer')
const log = require('ololog')
const beep = require('beeper')

// serial port settings
const SERIAL_PORT = process.env.SERIAL_PORT || '/dev/tty.usbserial-DN021J3C'
const SERIAL_BAUD = 115200

// how many bytes of data to send in each wireless packet
const CHUNK_SIZE = 200

// Just do it!
start()

async function start() {
  logAction('Opening serial connection')
  const port = new SerialPort(SERIAL_PORT, {
    baudRate: SERIAL_BAUD,
  })

  // log output from the port
  port.on('data', data => {
    let output = data.toString().trim()

    if (output.length) {
      output = output.replace('\n', '\n<- ')
      console.log('<-', output)
    }
  })

  // handle surprise exits
  registerSigint(port)

  // close the serial port on errors
  await main(port).catch(e => {
    log(e)
    port.close()
    process.exit(1)
  })

  port.close()
}

async function main(port) {
  const device = Number(process.argv[2])
  const cmd = process.argv[3]

  logAction('Waiting for device')
  await streamWait(port, 'INIT_OK', 'INIT_ERR')

  logAction(`Sending ${cmd} to device ${device}`)
  await sendPacket(port, device, cmd)

  // logAction('Blinking LED')
  // await sendPacket(port, 'L')
  //
  // await pause(500)
  //
  // logAction('Turing port 1 on')
  // await sendPacket(port, 2, 'S1')
  // beep(1)
  //
  // await pause(500)
  //
  // logAction('Turing port 1 off')
  // await sendPacket(port, 2, 'E1')
  // beep(2)

  logAction('All done!')
}

async function sendPacket(port: Object, device: number, data) {
  // enter input mode
  await writeWait(port, 'MSG_START' + String.fromCharCode(device), 'READY')

  // watch for input errors, but don't hold up the process
  streamWait(port, undefined, 'INPUT_ERR', false)

  // write the packet body
  await write(port, data)

  // end the packet
  await writeWait(port, 'MSG_SEND', 'SEND_OK', 'SEND_ERR', 60000)
}

function writeWait(port: Object, data, ...params) {
  return Promise.all([streamWait(port, ...params), write(port, data)]).then(
    r => r[0],
  )
}

function write(port: Object, data) {
  return new Promise((resolve, reject) => {
    console.log('->', data)

    port.write(data, err => {
      if (err) reject(err)
      else resolve()
    })
  })
}

function streamWait(
  port,
  searchStrings: string | Array<string> = [],
  errStrings: string | Array<string> = [],
  timeout = 10000,
) {
  return new Promise((resolve, reject) => {
    if (typeof searchStrings === 'string') {
      searchStrings = [searchStrings]
    }
    if (typeof errStrings === 'string') {
      errStrings = [errStrings]
    }

    let timer
    if (timeout !== false) {
      timer = setTimeout(() => {
        port.removeListener('data', cc)
        reject(
          'No response from serial port!  Expecting one of: ' +
            // $FlowFixMe
            searchStrings.join(', '),
        )
      }, timeout)
    }

    let allMatches = new Map([
      ...searchStrings.map(string => {
        return [string, []]
      }),
      ...errStrings.map(string => {
        return [string, []]
      }),
    ])

    function cc(chunk) {
      // log('Processing chunk', chunk.toString().trim(), '==>')

      for (const char of chunk.toString()) {
        // log(char.trim(), '=>')

        allMatches.forEach((matches, string) => {
          // process existing matches
          const newMatches = []
          matches.forEach(i => {
            i++
            if (char === string[i]) {
              // we still have a match
              if (i === string.length - 1) {
                // resolve the promise!
                port.removeListener('data', cc)
                clearTimeout(timer)
                if (errStrings.includes(string)) {
                  reject(`Got error from device: ${string}`)
                } else {
                  resolve()
                }
              } else {
                // add it to the list
                newMatches.push(i)
              }
            }
          })
          allMatches.set(string, newMatches)

          // check if we're at the start of a new match
          if (char === string[0]) {
            // start of a new potential match
            // $FlowFixMe
            allMatches.get(string).push(0)
          }
        })
      }
    }

    port.on('data', cc)
  })
}

function registerSigint(port) {
  process.on('SIGINT', () => {
    console.log('Gracefully closing the serial port')

    port.close(() => {
      process.exit()
    })
  })
}

function logAction(...props) {
  return console.log(chalk.blue('==>', ...props))
}

function pause(delay) {
  return new Promise(resolve => {
    setTimeout(resolve, delay)
  })
}
