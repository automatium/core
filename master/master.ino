// #include "Buffer.h"
#include "../lib/RadioConfig.cpp"
#include "../lib/RollingBuffer.cpp"
#include <RHReliableDatagram.h>
#include <RH_RF95.h>
#include <SPI.h>

#define SERVER_ADDRESS 1

// commands and messages
#define CMD_MAX_LEN 25

#define IPS(i) i, sizeof(i) - 1

// Singleton instance of the radio driver
RH_RF95 driver;

// Class to manage message delivery and receipt, using the driver declared above
RHReliableDatagram manager(driver, SERVER_ADDRESS);

void matchMsg() {}

void setup() {
  Serial.begin(115200);

  if (!manager.init())
    Serial.println("INIT_ERR");
  else
    Serial.println("INIT_OK");

  // configure driver
  driver.setModemConfig(MODEM_PRESET);
  driver.setFrequency(FREQ);
  driver.setPreambleLength(TX_PREAMBLE, ACK_PREAMBLE);
}

// Serial input buffer
RollingBuffer<CMD_MAX_LEN, true> serin;

void loop() {
  // read radio
  if (manager.available()) {
    Serial.println("RX_START");

    uint8_t rxbuf[RH_RF95_MAX_MESSAGE_LEN];
    uint8_t rxlen = sizeof(rxbuf);
    uint8_t from;
    if (manager.recvfrom(rxbuf, &rxlen, &from)) {
      Serial.write((char *)rxbuf);
      Serial.println("");
      Serial.println("RX_END");
    } else {
      Serial.println("RX_ERR");
    }
  }

  // read serial port
  if (Serial.available()) {
    serin.write(Serial.read());
    // check for send command
    if (serin.matchTail(IPS("MSG_START"))) {
      // wait for next char
      while (!Serial.available()) {
        // do nothing
      }

      // next char is dst addr
      uint8_t dst_addr = Serial.read();

      // entering packet recieve mode
      RollingBuffer<RH_RF95_MAX_MESSAGE_LEN, false> tx;
      Serial.println("\nREADY");

      while (1) {
        if (Serial.available()) {
          // add to the buffer
          if (tx.write(Serial.read())) {
            // check for send command
            if (tx.matchTail(IPS("MSG_SEND"))) {
              Serial.println("\nSEND_PENDING");

              // prepare to send
              unsigned long start = millis();
              bool status = true;

              while (millis() - start < TX_TIME) {
                if (!manager.sendto((uint8_t *)tx.buf, tx.length - 8,
                                    dst_addr)) {
                  status = false;
                  break;
                }
              }

              // print status
              if (status) {
                // wait for the ACK packet
                start = millis();
                uint8_t ackbuf[2] = {0, 0};
                uint8_t len = sizeof(ackbuf);
                uint8_t from;
                while (millis() - start < ACK_TIMEOUT) {
                  if (manager.recvfrom(ackbuf, &len, &from) &&
                      from == dst_addr && ackbuf[0] == 'O' &&
                      ackbuf[1] == 'K') {
                    Serial.println("\nSEND_OK");
                    // return to mail loop
                    return;
                  }
                }

                // must not have gotten an ACK
                Serial.println("\nSEND_ERR");

              } else {
                Serial.println("\nSEND_ERR");
              }
            } else if (tx.matchTail(IPS("MSG_CANCEL"))) {
              Serial.println("\nCANCEL_OK");
            }
          } else {
            Serial.println("\nINPUT_ERR");
            break;
          }
        }
      }
    }
  }
}
