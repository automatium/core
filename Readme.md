# Automatium

Right now we're running standard OpenSprinkler for the UI with a secondary Node server to receive HTTP commands and send them wirelessly. A custom GUI is WIP.

## Quick Rpi Setup

Tested on a Raspberry Pi 3 running Raspbian Jessie. Internet connection is
required. ;)

Node server runs on port 3000. OpenSprinkler runs on port 8080.

```shell
# Clone repo
git clone https://gitlab.com/szaver/sprinkler.git
cd sprinkler

# Compile and install OpenSprinkler
cd OpenSprinkler-Firmware
sudo ./build.sh ospi # answer yes to the run at startup question
cd ..

# install Node (the Pi's version of Node is terribly out of date)
curl -o- https://raw.githubusercontent.com/creationix/nvm/v0.33.6/install.sh | bash
source ~/.profile # reload the updated $PATH so NVM will be available immediately
nvm install node

# install Yarn
curl -o- -L https://yarnpkg.com/install.sh | bash
source ~/.profile

# Install Node dependencies
cd controller
yarn install

# Install PM2
yarn global add pm2
pm2 startup # follow the prompts

# Start the Node.js server
pm2 start dist/controller.js
pm2 save
```

## Valve Manager firmware

It's located in `decoder` and `master`. You need to have the Arduino IDE
installed.

You'll also need to change `Arduino.mk` to point to the right serial ports your system.

```shell
$ make upload
```

## How tasks are made

A task says, "run this valve for 30 second".

The backend needs to know:

* What do I need to do? (turn on or off)
* When will I do the next thing?

The frontend needs to know:

* What is the current port state? (Queued, Running, Stopping, Idle)

The basic task from the server perspective is:

```rust
struct Job {
  duration: Duration,
  started: Option<Date>,
}
```

A client can also infer everything it needs to know from this interface

## Todo List

- [x] +/- buttons for runtime
- [x] Homepage alignment
- [x] Server Sync (without blind overwrite)
- [x] Delete port
- [x] Cancel Job
- [ ] Failure alerts by email
- [ ] Schedule history
- [ ] Optimize webui

## Mosquitto cheatsheet

BTW, I want to switch to mosca so I'll have an 0MQ adapter, which will allow me to connect directly from Rust via 0MQ
