import React from "react"
import clsx from "clsx"

const SignalLevel = ({ level }) => {
  let bars = []
  for (let i = 1; i <= 4; i++) {
    let bg = "bg-gray-400"
    if (i <= level) {
      bg = "bg-gray-800"
    }
    bars.push(<div className={clsx("ml-1 w-2", bg, `h-` + i * 2)} />)
  }

  return <div className="ml-4 flex items-end">{bars}</div>
}

export default SignalLevel
