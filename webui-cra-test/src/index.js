import React from "react"
import ReactDOM from "react-dom"

import "typeface-merriweather"
import "typeface-source-sans-pro"
import "./index.css"

// import store from "./model/store.re"

import App from "./App.re"

ReactDOM.render(<App />, document.getElementById("root"))
