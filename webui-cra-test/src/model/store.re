type signal =
  | Zero
  | One
  | Two
  | Three
  | Four;
type valve = {
  signal,
  name: string,
};
type state = {valves: list(valve)};