import React from "react"

import { Link } from "react-router-dom"

import Header from "./Header.js"
import SignalLevel from "./lib/Signal"

const Valve = ({ id, signal }) => {
  return (
    <Link to={"/valve/" + id}>
      <div className="flex flex-col p-8 border-0 border-b border-gray-500">
        <div className="flex items-center">
          <h2 className="font-serif text-2xl font-bold text-gray-900">
            Sweet Pea Overhead
          </h2>
          <SignalLevel level={signal} />
        </div>
        <span className="font-semibold text-gray-800">Not Running</span>
        <span className=" text-gray-800">
          Last Run: Thursday, May 8 (2 days ago)
        </span>
      </div>
    </Link>
  )
}

const Valves = () => {
  return (
    <div className="flex flex-col font-sans">
      <Header title="Better Together Farm" />
      <Valve id="1" signal={2} />
      <Valve id="2" signal={4} />
      <Valve id="3" signal={1} />
      <Valve id="4" signal={3} />
      <Valve id="5" signal={4} />
    </div>
  )
}

export default Valves
