import React from "react"
import { Link } from "react-router-dom"

const Header = ({ title, back }) => {
  return (
    <div className="flex flex-col">
      <div className=" w-full h-16 bg-gray-800 flex items-center relative">
        {back && (
          <Link className="text-gray-200 text-lg absolute left-0 p-8" to={back}>
            back
          </Link>
        )}
        <h1 className="text-gray-100 font-bold font-serif text-3xl mx-auto">
          {title}
        </h1>
      </div>
      <div className="h-2 bg-blue-500" />
    </div>
  )
}

export default Header
