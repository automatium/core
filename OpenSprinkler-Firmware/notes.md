## Switch an HTTP station
call stack (inside-out)
switch_httpstation
switch_special_station
set_station_bit

- - -

## Respond to server request
inverted call stack (top => bottom)
server_json_controller
server_json_controller_main
