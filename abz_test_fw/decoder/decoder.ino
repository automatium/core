// rf95_reliable_datagram_client.pde
// -*- mode: C++ -*-
// Example sketch showing how to create a simple addressed, reliable messaging
// client with the RHReliableDatagram class, using the RH_RF95 driver to control
// a RF95 radio. It is designed to work with the other example
// rf95_reliable_datagram_server Tested with Anarduino MiniWirelessLoRa

#include "RadioConfig.cpp"
#include <RHReliableDatagram.h>
#include <RH_RF95.h>
#include <SPI.h>


// #define DEBUG

#define SERVER_ADDRESS 1
#define CLIENT_ADDRESS 3

// misc pins
#define LED 13

// driver pins
#define BOOST_EN 13
#define DRV_EN 12
#define A_ON 8
#define A_OFF 9


// solenoid delay
// #define ON_DELAY 15
#define ON_DELAY SLEEP_15MS
// #define OFF_DELAY 15
#define OFF_DELAY SLEEP_15MS

// Singleton instance of the radio driver
RH_RF95 driver;

// Class to manage message delivery and receipt, using the driver declared above
RHReliableDatagram manager(driver, CLIENT_ADDRESS);

void specialDigitalWrite(uint8_t port, uint8_t state)
{
#ifdef DEBUG
  Serial.print("Writing ");
  Serial.print(state);
  Serial.print(" to port ");
  Serial.print(port);
#endif

  digitalWrite(port, state);

#ifdef DEBUG
  Serial.println("...done.");
  delay(100);
#endif
}

// array of low pin / high tuples
#define PORT_COUNT 1
uint8_t portsTable[PORT_COUNT][3] = {{A_OFF, A_ON}};

void pulse_valve(uint8_t port, bool state) {
 uint8_t pin = LED;
 if (state) {
   pin = portsTable[port][1];
 } else {
   pin = portsTable[port][0];
 }

 // enable boost and driver
 digitalWrite(BOOST_EN, HIGH);
 digitalWrite(DRV_EN, HIGH);
 delay(100);

 digitalWrite(pin, HIGH);
 delay(10);
 digitalWrite(pin, LOW);

 // disable boost and driver to save power
 digitalWrite(BOOST_EN, LOW);
 digitalWrite(DRV_EN, LOW);
}

uint8_t charToPort(uint8_t b) { return b - 48 - 1; }

// handle command
bool handleCmd(uint8_t *buf)
{
  // check for S (start) command
  if (buf[0] == 'S')
    pulse_valve(*portsTable[charToPort(buf[1])], true);
  // check for E (end) command
  else if (buf[0] == 'E')
    pulse_valve(*portsTable[charToPort(buf[1])], true);
  else
    return false;

  return true;
}

void setup()
{

#ifdef DEBUG
  Serial.begin(115200);
  if (manager.init())
  {
    Serial.println("Decoder Ready!");
  }
  else
  {
    Serial.println("Init Error!");
  }
  delay(500);
#else
  manager.init();
#endif

  // configure driver
  driver.setModemConfig(MODEM_PRESET);
  driver.setFrequency(FREQ);
  driver.setPreambleLength(TX_PREAMBLE, ACK_PREAMBLE);

  pinMode(LED, OUTPUT);
  digitalWrite(LED, LOW);

  pinMode(BOOST_EN, OUTPUT);
  pinMode(DRV_EN, OUTPUT);
  digitalWrite(BOOST_EN, LOW);
  digitalWrite(DRV_EN, LOW);

  // init driver pins
  for (uint8_t i = 0; i < PORT_COUNT; i++)
  {
    for (uint8_t x = 0; x < 3; x++)
    {
      pinMode(portsTable[i][x], OUTPUT);
      digitalWrite(portsTable[i][x], LOW);

#ifdef DEBUG
      Serial.print("Init pin ");
      Serial.print(portsTable[i][x]);
      Serial.println(" as output.");
#endif
    }
  }
}

// rx buffer
uint8_t buf[2];

void loop()
{
#ifdef DEBUG
  Serial.println("Hello, World!");
  delay(100);
#endif

  // do a CAD check
  if (driver.isChannelActive())
  {

    // switch to RX mode
    driver.setModeRx();

#ifdef DEBUG
    Serial.println("Channel activity detected");
    delay(100);
#endif

    // wait for the packet to come in.
    // (must be longer than max airtime)
    delay(RX_DELAY);

    // attempt to recieve the incoming packet
    uint8_t len = sizeof(buf);
    uint8_t from;
    if (manager.recvfrom(buf, &len, &from) && from == SERVER_ADDRESS)
    {
      // immediately sleep the radio
      driver.sleep();

      // check for a valid command
      if (len == 2)
      {
        // attemtp to handle the command
        if (!handleCmd(buf))
        {
#ifdef DEBUG
          Serial.println("Invalid command!");
#endif
        }
      }
      // led blink command
      else if (len == 1 && buf[0] == 'L')
      {
        digitalWrite(LED, HIGH);
        delay(2000);
        digitalWrite(LED, LOW);
      }
      // unknown command
      else
      {
#ifdef DEBUG
        Serial.println("Invalid command!");
#endif
      }

      // ack the packet in any case
      delay(2000);
#ifdef DEBUG
      Serial.println("Sending ACK");
#endif
      uint8_t ack[2] = {'O', 'K'};
      manager.sendto(ack, 2, SERVER_ADDRESS);
    }
    else
    {
#ifdef DEBUG
      Serial.println("RX Err");
#endif
    }
  }

#ifdef DEBUG
  delay(100);
#endif

  // sleep for 8 seconds before checking again
  driver.sleep();
  delay(2000);
}
