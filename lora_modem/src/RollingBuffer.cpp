#include <Arduino.h>

template <uint8_t W, bool WRAP>
class RollingBuffer
{
public:
    uint8_t cursor;
    uint8_t length;
    char buf[W];

    RollingBuffer()
    {
        cursor = 0;
        length = 0;
    }

    // convert an absolute index into it's internal location
    uint8_t getIndex(uint8_t i)
    {
        if (length == W)
        {
            return (cursor + i) % W;
        }
        else
        {
            return i;
        }
    }

    // read a byte
    char read(uint8_t loc) { return buf[getIndex(loc)]; }

    // write a byte
    bool write(char byte)
    {
        // verify that the write is allowed
        if (!WRAP && length > cursor)
        {
            return false;
        }

        // write the byte
        buf[cursor] = byte;

        // update cursor/length
        if (length < ++cursor)
            length = cursor;
        cursor = cursor % W;

        return true;
    }

    bool matchTail(const char *pattern, uint8_t plen)
    {
        // Serial.println("MatchTail ==>");
        // Serial.println(plen);
        // Serial.println(length);
        // Serial.println(buf);

        // not enough chars to match
        if (plen > length)
        {
            return false;
        }

        // calculate starting index
        uint8_t start_index = length - plen;

        for (uint8_t i = 0; i < plen; i++)
        {
            if (read(start_index + i) != pattern[i])
            {
                return false;
            }
        }

        return true;
    }
};
