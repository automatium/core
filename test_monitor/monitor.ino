// rf95_reliable_datagram_client.pde
// -*- mode: C++ -*-
// Example sketch showing how to create a simple addressed, reliable messaging
// client with the RHReliableDatagram class, using the RH_RF95 driver to control
// a RF95 radio. It is designed to work with the other example
// rf95_reliable_datagram_server Tested with Anarduino MiniWirelessLoRa

#include "../lib/RadioConfig.cpp"
#include <LowPower.h>
// #include <RHReliableDatagram.h>
#include <RH_RF95.h>
#include <SPI.h>
// #include <SPIFlash.h>
// #include <avr/wdt.h>

#define SERVER_ADDRESS 1
#define CLIENT_ADDRESS 2

// misc pins
#define LED 9

// Singleton instance of the radio driver
RH_RF95 driver;

// Class to manage message delivery and receipt, using the driver declared
// above RHReliableDatagram manager(driver, CLIENT_ADDRESS);

void printPadding(uint8_t number, uint8_t width, char padChar,
                  uint8_t base = 10) {
  int currentMax = base;
  for (uint8_t i = 1; i < width; i++) {
    if (number < currentMax) {
      Serial.print(padChar);
    }
    currentMax *= base;
  }
}

void leftPadHex(uint8_t number, uint8_t width) {
  printPadding(number, width, '0', 16);
  Serial.print(number, HEX);
}

void rightPad(uint8_t number, uint8_t width) {
  Serial.print(number, DEC);
  printPadding(number, width, ' ');
}

void setup() {

  // configure manager (prob not needed on the client)
  // manager.setTimeout(TX_TIMEOUT);
  // manager.setRetries(TX_RETRIES);

  Serial.begin(115200);

  if (driver.init()) {
    Serial.println("Sniffing the air...");
  } else {
    Serial.println("Init failed!!");
  }

  // configure driver
  driver.setModemConfig(MODEM_PRESET);
  driver.setFrequency(FREQ);
  driver.setPreambleLength(TX_PREAMBLE, ACK_PREAMBLE);
  driver.setPromiscuous(true);
}

// rx buffer
uint8_t buf[RH_RF95_MAX_MESSAGE_LEN];

// output line counter
int output_lines = 0;

void loop() {
  while (1) {
    unsigned long last = millis();

    if (driver.available()) {
      uint8_t len = sizeof(buf);
      bool status = driver.recv(buf, &len);

      // maybe print line headers
      if (!(output_lines++ % 10)) {
        Serial.println("");
        Serial.println("Status  Time    From  To  ID  Flags  Body");
        Serial.println("-----------------------------------------");
      }

      // print status
      if (status) {
        Serial.print("OK      ");
      } else {
        Serial.print("ERR     ");
      }

      // print time
      unsigned long now = millis();
      Serial.print("+");
      if (now - last > 1000) {
        unsigned long val = millis() - last / 1000.0;
        Serial.print(val);
        Serial.print("s");
        printPadding(val, 6, ' ');
      } else {
        unsigned long val = millis() - last;
        Serial.print(val);
        Serial.print("ms");
        printPadding(val, 5, ' ');
      }

      // Print metadata
      rightPad(driver.headerFrom(), 7);
      rightPad(driver.headerTo(), 4);
      rightPad(driver.headerId(), 4);

      Serial.print("0x");
      leftPadHex(driver.headerFlags(), 2);
      Serial.print("   ");

      // print buffer contents
      for (uint8_t i = 0; i < len; i++) {
        leftPadHex(buf[i], 2);
        Serial.print(" ");
      }

      // end the line
      Serial.println("");
    }
  }
}
