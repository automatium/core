EESchema Schematic File Version 4
EELAYER 29 0
EELAYER END
$Descr A4 11693 8268
encoding utf-8
Sheet 1 1
Title ""
Date ""
Rev ""
Comp ""
Comment1 ""
Comment2 ""
Comment3 ""
Comment4 ""
$EndDescr
$Comp
L moteino-eagle-import:MOTEINO_M0SMD U$1
U 1 0 20F524AA4D826FCC
P 5500 4100
F 0 "U$1" H 5500 4100 50  0001 C CNN
F 1 "MOTEINO_M0SMD" H 5500 4100 50  0001 C CNN
F 2 "moteino:MOTEINO_M0_LONGPADS" H 5500 4100 50  0001 C CNN
F 3 "" H 5500 4100 50  0001 C CNN
	1    5500 4100
	1    0    0    -1  
$EndComp
$EndSCHEMATC
